# Data extraction scripts

This repository contains various ad-hoc scripts that I create for the purposes
of various data extraction tasks that I don't want to throw away. If you are
going to use this, use with caution, as the scripts are written specifically for
certain tasks and are not generically applicable. Moreover, you might need to
install various tools and have the environment setup correctly for the scripts
to work.
