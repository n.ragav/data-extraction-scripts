import sys
import csv

if len(sys.argv) != 2:
    sys.exit(1)

record_count=30

inputfile = sys.argv[1]

with open(inputfile, "r") as f:
    lines = f.read().splitlines()

print(f"Processing {inputfile} ...")

linecount = 0
marker=0
batchcount=0
records = []
record_lines = set()
state = lines[2]
params = lines[4:34]
group_codes = []

for line in lines:
    linecount += 1
    if line.strip() == "":
        if batchcount == record_count:
            records.append(lines[marker:linecount-1])
            record_lines.update(range(marker, linecount-1))
        batchcount=0
        marker = linecount
    else:
        batchcount += 1

for line_no in range(34,len(lines)):
    if line_no not in record_lines:
        text = lines[line_no].strip().lower()
        if text =='all' or text == 'other':
            group_codes.append(lines[line_no])
        else:
            try:
                code = int(text)
                group_codes.append(code)
            except:
                pass

output_list = []
for i in range(len(group_codes)):
    output_list.append([group_codes[i], state, records[i][0], records[i][18]])

with open(f"{state}.csv", "a") as csvfile:
    csvwriter = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
    csvwriter.writerows(output_list)

print(f"Output written to {state}.csv")
