import sys
import pandas as pd

if len(sys.argv) != 2:
    sys.exit(1)

inputfile = sys.argv[1]
df = pd.read_csv(inputfile, header=None)
indices = [0, 1, 19]

df.loc[indices].T.to_csv(f"{inputfile}.csv.T", header=False, index=False)
