#!/bin/bash

# This script was written as part of a data analysis project I did in
# fulfilment of the requirement of a data analytics course at IIM Bangalore.
#
# It extracts specific data from the Annual Survey of Industries report
# available under https://microdata.gov.in/nada43/index.php/catalog/ASI. The
# report that this script concerns with is this one 
# https://microdata.gov.in/nada43/index.php/catalog/158/download/2034
#
# This script assumes that you have several tools installed and setup in your
# environment, including, but not limited to pdftk, pdftotext (poppler), and any
# other supporting libraries that the script might need. 
#
# If you'd like to use this script but find that it doesn't work as expected,
# contact me or leave a comment. I'll try to help.
#
# I'm not responsible for any inadvertent data loss that could happen as a
# result of you running this script. Please read the script carefully and get
# clarity before you execute it.

# Abort on error
set -e

# You need to give the input file name, the report start page and end page in
# the file.
report_file="Volume I 2019-20 FINAL.pdf"
report_start_page=617
report_end_page=673
finalfile="statewise_groupwise_fuel_consumption"

# The number of data columns in the input report, excluding the group column
num_data_columns=7

echo "Cleaning up existing files"
rm -f pg_*.pdf pg_*.txt "${finalfile}.*" *.1 *.2

pdftk "${report_file}" cat ${report_start_page}-${report_end_page} \
    output extracted.pdf

echo "Splitting the pdf into individual pages"
pdftk extracted.pdf burst
pagecount=$(ls -1 pg_*.pdf| wc -l)
echo "${pagecount} pages extracted"
rm -f extracted.pdf doc_data.txt

echo "Converting the extracted pdf into text files"
ls -1 pg_*.pdf | xargs -n 1 pdftotext

# The following code extracts data from each file into its own CSV file
allfiles=($(ls -1 pg_*.txt))
for filename in ${allfiles[@]}; do
    echo "Processing file: ${filename}"
    ofname=$(sed -n 5,5p ${filename})

    echo "Output will be written to ${ofname}.csv"
    # Gets the line number from where the data begins. Also the line number from
    # 10 until this point is where the industry codes are.
    startmeta=10
    datarow=$(grep -n '^$' ${filename} | sed -n 2,2p | tr ':' ' ')
    sed -n "${startmeta}"','"$((datarow-1))"'p' ${filename}  > "${ofname}.1"
    # The pr command transforms the row data into columnar data every 
    # ${num_data_columns} rows.
    sed -n "${datarow}"',$p' ${filename} | sed -n '/^[0-9][0-9]*$/p' \
        | pr -${num_data_columns}ts',' > "${ofname}.2"
    paste -d ',' "${ofname}.1" "${ofname}.2" >> "${ofname}.csv"
done

# The following code puts the names of the states as the first column in the
# files and merges all the files into one csv file, namely, output.csv
OLDIFS=$IFS
IFS=$(echo -en "\n\b")
ofiles=($(ls -1 *.csv))
for f in ${ofiles[@]}; do
    echo "Removing the last row from ${f}"
    g=$(echo $f | sed 's/&/\\\&/;s/.csv$//')
    sed '$d;s/^/"'${g}'",/' "${f}" > "${f}.1"
done
IFS=$OLDIFS


cat *.csv.1 > "${finalfile}.tmp"

echo "Cleaning up"
rm -f *.csv *.txt *.1 *.2 pg_*.pdf

echo "All tasks completed. Output available in ${finalfile}.csv"
mv "${finalfile}.tmp" "${finalfile}.csv"
